# moviedb_flutter_app

Une application flutter pour les films

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## Memo - Start Emulator with dns-server
    cd C:\Users\nyasse\AppData\Local\Android\sdk\tools
    emulator.exe -avd Nexus_6_API_25 -dns-server 8.8.8.8