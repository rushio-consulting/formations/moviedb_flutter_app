import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:flutter/material.dart';
import 'package:moviedb_flutter_app/src/api/api.dart' as rest_api;
import 'package:moviedb_flutter_app/src/models/movie.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:share/share.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool loading = true;
  List<Movie> movies = [];
  sqflite.Database database;

  @override
  void initState() {
    super.initState();
    _loadMovies();
  }

  Future<void> _loadMovies() async {
    final databasePath = await sqflite.getDatabasesPath();
    final path = join(databasePath, 'formation.db');

    database = await sqflite.openDatabase(path, version: 1,
        onCreate: (db, version) async {
      await db.execute(
          'CREATE TABLE favorites (id INTEGER PRIMARY KEY, title TEXT)');
    });

    MoviesResponse movieResponse = await rest_api.topRatedMovies();
    movies = movieResponse.movies;
    _loadFavorites();
  }

  Future<void> _loadFavorites() async {
    final data = await database.rawQuery('SELECT * FROM favorites');
    final moviesFromDb = data.map((d) => Movie.fromJson(d)).toList();

    for (final mDb in moviesFromDb) {
      final movie =
          movies.firstWhere((m) => m.id == mDb.id, orElse: () => null);
      movie?.favorite = true;
    }

    setState(() {
      loading = false;
    });
  }

  Future<void> _onTapMovie(Movie m) async {
    for (final movie in movies) {
      if (movie.id == m.id) {
        final favoriteState = movie.favorite;
        setState(() {
          movie.favorite = !movie.favorite;
        });

        if (!favoriteState) {
          await database.rawInsert(
              'INSERT INTO favorites (id, title) VALUES (?,?)',
              [m.id, m.title]);
        } else {
          await database
              .rawDelete('DELETE FROM favorites WHERE id = ? ', [m.id]);
        }

        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    _handleShowSettings() => Navigator.popAndPushNamed(context, '/settings');

// Drawer is a ListView with entries
    Widget _buildDrawer(BuildContext context) {
      return Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
                child: Container(
                    child: Image.asset(
              "images/flutter.png",
              fit: BoxFit.cover,
            ))),
            const ListTile(
              leading: const Icon(Icons.assessment),
              title: const Text('Home'),
              selected: true,
            ),
            const Divider(),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Settings'),
              onTap: _handleShowSettings,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      // Build APP BAR
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share('Hello from ${widget.title}');
            },
          )
        ],
      ),

      drawer: _buildDrawer(context),

      // Build BODY
      body: loading
          ? Center(child: new CircularProgressIndicator())
          : ListView.builder(
              padding: EdgeInsets.all(8.0),
              itemCount: movies.length,
              itemBuilder: (BuildContext context, int index) {
                return MovieWidget(movies[index], _onTapMovie);
              },
            ),
    );
  }
}

typedef Future<void> OnTapMovie(Movie m);

// Widget sans état
class MovieWidget extends StatelessWidget {
  // Référence vers la méthode OnTapMovie
  final OnTapMovie onTap;

  final Movie movie;

  // Constructeur à mettre à jour
  // On fournit la méthode onTap
  MovieWidget(this.movie, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 284,
                child: Image.network(
                  'http://image.tmdb.org/t/p/w185/${movie.posterPath}',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  movie.title,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
              ),
              Text(
                movie.overview,
                style: TextStyle(color: Colors.black54),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.calendar_today),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(
                          movie.releaseDate,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        movie.favorite ? Icons.star : Icons.star_border,
                        color: movie.favorite ? Colors.yellow : Colors.black,
                      ),
                      onPressed: () {
                        onTap(movie);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
