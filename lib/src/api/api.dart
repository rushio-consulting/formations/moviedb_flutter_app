import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:moviedb_flutter_app/src/models/movie.dart';

const String API_KEY = '4205ec1d93b1e3465f636f0956a98c64';
const String API = 'https://api.themoviedb.org/3';

Future<MoviesResponse> topRatedMovies() async {
  final String urlPath = 'movie/top_rated';
  print("$API/$urlPath?api_key=$API_KEY&language=fr");
  final http.Response response =
  await http.get('$API/$urlPath?api_key=$API_KEY&language=fr');
  Map data = json.decode(response.body);
  data['tmdb'] = "true";
  return MoviesResponse.fromJson(data);
}


